# -*- coding: utf-8 -*-
"""
LIBiAM Biomechanics Lab
CENUR Litoral Norte
Universidad de la Republica

Created on Tue May 18 2024
@authors: german and carlo
Depts. of Biological Engineering and Biological Sciences
"""
def dutyf(stride,stride_time,fs,gait):
    """Compute Duty Factors """
    import numpy as np  # Required numpy ver 1.20.3
    import pandas as pd # Required pandas ver < 2.0.0
    import math
    import os

    def df(sx,side):
        threshold = sx.loc[min[side]] + 10  
        threshold = math.ceil(threshold)
        
        # Shift the series to compare current and previous values
        shifted_series = sx.shift(1)
        # Find where the value is increasing across the threshold
        increasing_crosses = (sx > threshold) & (shifted_series <= threshold)
        # Find where the value is decreasing across the threshold
        decreasing_crosses = (sx < threshold) & (shifted_series >= threshold)
        # Get the positions (indices) of the increasing crosses
        increasing_positions = increasing_crosses[increasing_crosses].index.tolist()
        # Get the positions (indices) of the decreasing crosses
        decreasing_positions = decreasing_crosses[decreasing_crosses].index.tolist()

        if len(increasing_positions) == 1 and len(decreasing_positions) == 1:
            interval = increasing_positions[0] - decreasing_positions[0]
            if interval > 0:
                DF = interval / len(sx)
            else:
                DF = (len(sx) + interval) / len(sx)
        else:
            DF = 0
        
        return DF

    # -------------------------------------------------------------------------
    
    headers = list(stride.columns)
    mtz = [s for s in headers if 'MTz' in s]
    [R,L] = mtz
    s = stride[mtz]
    idxInicio = s.index[0]
    idxFin = s.index[-1]
    min = s.idxmin()
    max = s.idxmax()

    sx = s[R]
    RDF = df(sx,R)

    sx = s[L]
    LDF = df(sx,L)
    
      
    # -------------------------------------------------------------
    return RDF,LDF
    
