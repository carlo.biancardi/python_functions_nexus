# -*- coding: utf-8 -*-
"""
LIBiAM Biomechanics Lab
CENUR Litoral Norte
Universidad de la Republica

Created on Tue May 14 2024
From script "energetics.py" 
@author: german and carlo
Depts. of Biological Engineering and Biological Sciences
"""
def energetics(file, folder_path):
    """Compute WEXT, Recovery, etc. """
    import numpy as np  # Required numpy ver 1.20.3
    import pandas as pd # Required pandas ver < 2.0.0
    import matplotlib.pyplot as plt
    import matplotlib.ticker as mtick            # carlo 20240518
    from scipy import signal
    import glob
    import os

    # Local functions
    from analyses.duty import dutyf
    from analyses.wint import wint
    
    directory = os.path.join(folder_path, 'energies_plots')
    if not os.path.exists(directory):
        os.makedirs(directory)
    #------------------------------------    
    file_id = os.path.basename(file)[:-4]	    
    #----------------------------------------------------------
	# Carga datos
    data = np.load(file, encoding='latin1',allow_pickle=True).item()
    masa = [s for s in data['info'].columns if "mass" in s]
    m = float(data.get('info')[masa].values.item())
    # velocidad = [s for s in data['info'].columns if "vel" in s] #20240508 Carlo new pickle/numpy file
    vel = np.round(float(data.get('info')['vel'].values.item())/3.6,2)
    veltype = data.get('info')['veltype'].values[0]
    # fsample = [s for s in data['info'].columns if "fs_kin" in s] #20240508 Carlo new pickle/numpy file
    fs = float(data.get('info')['fsample'].values.item())
    dt = 1/fs
    gait = data.get('info')['gait'].values[0]
    subjectID = data.get('info')['subjectID'].values[0]
	#----------------------------------------------------------
	
	#----------------------------------------------------------
	# Crea ventana de convolución para cálculo de velocidad
    win = np.array([1/(2*dt),0,-1/(2*dt)])
	#----------------------------------------------------------
	
    dfs=[]
    r = []
    listLimbdf = []

   
    #energies_dir = os.path.join(directory, 'energies_dir')
    #if not os.path.exists(energies_dir):
    #    os.makedirs(energies_dir)
    for n,ii in enumerate(data.get('model_outputs')):
		#----------------------------------------------------------
		# Carga datos del centro de masa
        cm = ii[['cm_x','cm_y','cm_z']].reset_index(drop=True)/1000
        cm.cm_z = cm.cm_z.subtract(cm.cm_z.min())
		#----------------------------------------------------------
		# Calcula velocidad del centro de masa
        dcm = pd.DataFrame()
        dcm['x'] = signal.convolve(cm['cm_x'],win,'same')
        dcm['y'] = signal.convolve(cm['cm_y'],win,'same')
		# dcm.y = dcm.y.subtract(dcm.y.min())
        dcm['z'] = signal.convolve(cm['cm_z'],win,'same')
        dcm.iloc[0] = dcm.iloc[1]
        dcm.iloc[-1] = dcm.iloc[-2]
		#----------------------------------------------------------

		#----------------------------------------------------------
		# Calcula parámetros espaciotemporales
        stride_time = cm.shape[0]*dt
        strideFreq = 1/stride_time
        strideLength = vel/strideFreq
		#----------------------------------------------------------
        # Load marker trajectories
        stride = (data['kinematic'])[n]
        [rdf,ldf] = dutyf(stride,stride_time,fs,gait)
		#----------------------------------------------------------

		#----------------------------------------------------------
		# Cálculo de energías
        E = pd.DataFrame()
        E['Ekx'] = m*(dcm['x']**2)/2
        E['Ekx'] = E['Ekx'] - E['Ekx'].min()
		
        E['Eky'] = (m*(dcm['y']+vel)**2)/2
        E['Eky'] = E['Eky'] - E['Eky'].min()
		
        E['Ekz'] = (m*(dcm['z']**2))/2
        E['Ekz'] = E['Ekz'] - E['Ekz'].min()
		
        E['mgh'] = m*9.8*(cm['cm_z'])
        E['mgh'] = E['mgh'] - E['mgh'].min()
		
        E['Ev'] =  E['mgh'] + E['Ekz']
        E['Eh'] = E['Ekx'] + E['Eky']
		
        E['Etot'] = E['Ev'] + E['Eh']
		#----------------------------------------------------------

		#----------------------------------------------------------
		# Dibuja energias
        #plot = E[['Etot','Ev','Eh']].plot()            # carlo 20240518
        #fig = plot.get_figure()                        # carlo 20240518
        fig, ax = plt.subplots()
        ax.plot(E.Etot, label = 'Total Energy')        # carlo 20240518
        ax.plot(E.Ev, label = 'Potential Energy')      # carlo 20240518 
        ax.plot(E.Eh, label = 'Kinetic Energy')        # carlo 20240518
        plt.xlabel("Stride (frames)")                   # carlo 20240518
        plt.ylabel("Energies (J)")                      # carlo 20240518
        plt.legend(loc='upper center', fontsize='small')# carlo 20240518        
        fname = "energies_"+file_id+str(n+1)+".png"     # carlo 20240518
        energies_dir = os.path.join(directory, fname)   # carlo 20240518
        plt.savefig(energies_dir)                       # carlo 20240518 
        plt.close(fig)                                       # carlo 20240519
		#----------------------------------------------------------

		#----------------------------------------------------------
		# Cálculo de Wext, Wh, Wv y recovery
        W = pd.DataFrame()
        wtot = E['Etot'].diff()
        wtot.iloc[0] = wtot.iloc[1]
		
        wz = E['Ev'].diff()
        wz.iloc[0] = wz.iloc[1]
		
        wy = E['Eh'].diff()
        wy.iloc[0] = wy.iloc[1]

        Wtabs = abs(wtot)  # carlo 20240517 Instant Recovery
        Wvabs = abs(wz)    # carlo 20240517 Instant Recovery
        Whabs = abs(wy)    # carlo 20240517 Instant Recovery

        W['Wtot'] = wtot*(wtot>0)
        W['Wz'] = wz*(wz>0)
        W['Wy'] = wy*(wy>0)
        W['Rinst'] = (1-(Wtabs/(Wvabs+Whabs)))  # carlo 20240517 Instant Recovery

        Wext = W.Wtot.sum()/(strideLength*m)
        Wv = W.Wz.sum()/(strideLength*m)
        Wh = W.Wy.sum()/(strideLength*m)
        R = (1-(Wext/(Wv+Wh)))*100
        InstR = (W.Rinst.mean())*100   # carlo 20240517 Instant Recovery
        cg = (wz>0) == (wy>0)          # carlo 20240517 Congruency
        Cong = (sum(cg)/len(cg)) * 100 # carlo 20240517 Congruency

        #----------------------------------------------------------
		# Plot Recovery
        fig, bx = plt.subplots()
        bx.plot(W.Rinst, label = 'Inst. Recovery')     # carlo 20240518
        plt.gca().yaxis.set_major_formatter(mtick.PercentFormatter(xmax=1.0))
        plt.xlabel("Stride (frames)")                   # carlo 20240518
        plt.ylabel("Recovery")                          # carlo 20240518
        # plt.legend(loc='upper center', fontsize='small')# carlo 20240518        
        fname = "recovery_"+file_id+str(n+1)+".png"     # carlo 20240518
        energies_dir = os.path.join(directory, fname)   # carlo 20240518
        plt.savefig(energies_dir)                       # carlo 20240518
        plt.close(fig)                                       # carlo 20240519
        
        #----------------------------------------------------------
        energetic = pd.DataFrame()
        energetic['subjectID'] = [subjectID]
        energetic['BodyMass'] = [m]  # carlo 20240517
        energetic['gait'] = [gait]
        energetic['vel'] = [vel]
        energetic['veltype'] = [veltype]
        energetic['stride'] = [n]
        energetic['strideFreq'] = [strideFreq]
        energetic['strideLength'] = [strideLength]
        energetic['Df_right'] = [rdf]
        energetic['Df_left'] = [ldf]
        energetic['Wext'] = [Wext]
        energetic['Wh'] = [Wh]
        energetic['Wv'] = [Wv]
        energetic['recovery'] = [R]
        energetic['instantRec'] = [InstR]  # carlo 20240517 Instant Recovery
        energetic['Congruency'] = [Cong]  # carlo 20240517

        #---------------------------------------------------------
        
        Limbdf = wint(ii,stride,strideLength,m,dt)
        
		#----------------------------------------------------------
        #dfs.append(energetic)
        #chico = pd.concat(dfs,ignore_index=True)
        #wintstr = pd.DataFrame(listLimbdf)
        
	# datas = pd.concat(dfs,ignore_index=True).pivot_table(index= ['subjectID','gait','vel'])
    datas = pd.concat([energetic,Limbdf],axis=1)
    r.append(datas)

    return r
     