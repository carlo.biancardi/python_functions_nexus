# -*- coding: utf-8 -*-
"""
LIBiAM Biomechanics Lab
CENUR Litoral Norte
Universidad de la Republica

Created on Tue May 18 2024
From script "wint.py" 
@author: german and carlo
Depts. of Biological Engineering and Biological Sciences
"""
def wint(model,stride,strideLength,masa,dt):
    """Compute WINT """
    import numpy as np  # Required numpy ver 1.20.3
    import pandas as pd # Required pandas ver < 2.0.0
    import matplotlib.pyplot as plt
    import scipy.signal as sig
    import glob
    import seaborn as sns
    import os
    # Local functions
    import analyses.kinematics as kin


    # ----------------- DEFINITIONS --------------------------------
    segmentos = ['brazo_der', 'brazo_izq','antbrazo_der','antbrazo_izq',
                 'muslo_der','muslo_izq','pierna_der','pierna_izq',
                 'pie_der','pie_izq','tronco_der','tronco_izq']

    
    segmentosXYZ = ['pierna_der_x',  'pierna_der_y', 'pierna_der_z',
                    'pierna_izq_x', 'pierna_izq_y', 'pierna_izq_z',
                    'muslo_der_x', 'muslo_der_y', 'muslo_der_z', 
                    'muslo_izq_x', 'muslo_izq_y', 'muslo_izq_z',
                    'pie_der_x', 'pie_der_y', 'pie_der_z',
                    'pie_izq_x', 'pie_izq_y', 'pie_izq_z',
                    'brazo_der_x', 'brazo_der_y', 'brazo_der_z', 
                    'brazo_izq_x', 'brazo_izq_y', 'brazo_izq_z',
                    'antbrazo_der_x', 'antbrazo_der_y', 'antbrazo_der_z', 
                    'antbrazo_izq_x', 'antbrazo_izq_y', 'antbrazo_izq_z',
                    'tronco_der_x', 'tronco_der_y', 'tronco_der_z',
                    'tronco_izq_x', 'tronco_izq_y', 'tronco_izq_z']
    SegmentosDerechos = [s for s in segmentosXYZ if 'der' in s]
    SegmentosIzquierdos = [s for s in segmentosXYZ if 'izq' in s]

    # DEMPSTER partial masses
    m=pd.DataFrame(data = {'brazo_der':[masa*0.028],'brazo_izq':[masa*0.028],
                          'antbrazo_der':[masa*0.016],'antbrazo_izq':[masa*0.016],
                          'muslo_der':[masa*0.100],'muslo_izq':[masa*0.100],
                          'pierna_der':[masa*0.0465],'pierna_izq':[masa*0.0465],
                          'pie_der':[masa*0.0145],'pie_izq':[masa*0.0145],
                          'tronco_der':[masa*0.2890],'tronco_izq':[masa*0.2890]})
    
    # ------------------------------------------------------------------------------
           
    # Defino ventana de convolucion
    win = np.array([1/(2*dt),0,-1/(2*dt)])

    listaWint = []
    listLimbdf = []
    WintLimbByframe = []
    
    stride = stride / 1000                # from mm to m 
    N = len(stride)

    # Radius of gyration of the segments
    k=pd.DataFrame(data = {'brazo_der':[0.542]*np.sqrt((stride['RELBx']-stride['RSHx'])**2+(stride['RELBy']-stride['RSHy'])**2+(stride['RELBz']-stride['RSHz'])**2),
                            'brazo_izq':[0.542]*np.sqrt((stride['LELBx']-stride['LSHx'])**2+(stride['LELBy']-stride['LSHy'])**2+(stride['LELBz']-stride['LSHz'])**2),
                            'antbrazo_der':[0.827]*np.sqrt((stride['RWRx']-stride['RELBx'])**2+(stride['RWRy']-stride['RELBy'])**2+(stride['RWRz']-stride['RELBz'])**2),
                            'antbrazo_izq':[0.827]*np.sqrt((stride['LWRx']-stride['LELBx'])**2+(stride['LWRy']-stride['LELBy'])**2+(stride['LWRz']-stride['LELBz'])**2),
                            'muslo_der':[0.540]*np.sqrt((stride['RKNx']-stride['RGTx'])**2+(stride['RKNy']-stride['RGTy'])**2+(stride['RKNz']-stride['RGTz'])**2),
                            'muslo_izq':[0.540]*np.sqrt((stride['LKNx']-stride['LGTx'])**2+(stride['LKNy']-stride['LGTy'])**2+(stride['LKNz']-stride['LGTz'])**2),
                            'pierna_der':[0.528]*np.sqrt((stride['RANx']-stride['RKNx'])**2+(stride['RANy']-stride['RKNy'])**2+(stride['RANz']-stride['RKNz'])**2),
                            'pierna_izq':[0.528]*np.sqrt((stride['LANx']-stride['LKNx'])**2+(stride['LANy']-stride['LKNy'])**2+(stride['LANz']-stride['LKNz'])**2),
                            'pie_der':[0.690]*np.sqrt((stride['RMTx']-stride['RHEEx'])**2+(stride['RMTy']-stride['RHEEy'])**2+(stride['RMTz']-stride['RHEEz'])**2),
                            'pie_izq':[0.690]*np.sqrt((stride['LMTx']-stride['LHEEx'])**2+(stride['LMTy']-stride['LHEEy'])**2+(stride['LMTz']-stride['LHEEz'])**2),
                            'tronco_der':[0.830]*np.sqrt((stride['RGTx']-stride['RSHx'])**2+(stride['RGTy']-stride['RSHy'])**2+(stride['RGTz']-stride['RSHz'])**2),
                            'tronco_izq':[0.830]*np.sqrt((stride['LGTx']-stride['LSHx'])**2+(stride['LGTy']-stride['LSHy'])**2+(stride['LGTz']-stride['LSHz'])**2),})
    k.reset_index(inplace=True, drop=True)   
    
    relativeCm = pd.DataFrame()
    relativeVelocity = pd.DataFrame()

    for seg in [SegmentosDerechos,SegmentosIzquierdos]:
        for se in seg:
            relativeCm[se] = (model['cm_'+se] - model['cm_'+se[-1]])/1000
            relativeVelocity[se] = sig.convolve(relativeCm[se],win,'same')
    relativeCm.reset_index(inplace=True, drop=True)
        
    relativeVelocity.iloc[0] = relativeVelocity.iloc[1]
    relativeVelocity.iloc[N-1] = relativeVelocity.iloc[N-2]
    relativeVelocity.reset_index(inplace=True, drop=True)

    cinetica  = pd.DataFrame()

    for segXYZ in relativeVelocity.columns:
        cinetica[segXYZ] = (1/2) * ((relativeVelocity[segXYZ]**2) * float(m[segXYZ[:-2]]))
    cinetica.reset_index(inplace=True, drop=True)

    angles = pd.DataFrame()
    angles_abs = pd.DataFrame()

    for lado,side in zip(['R','L'],['_der','_izq']):
        angles['brazo'+side] = kin.angulos(stride,lado+'SH',lado+'ELB',lado+'GT')
        angles['antbrazo'+side] = kin.angulos(stride,lado+'ELB',lado+'SH',lado+'WR')
        angles['muslo'+side] = kin.angulos(stride,lado+'GT',lado+'SH',lado+'KN') 
        angles['pierna'+side] = kin.angulos(stride,lado+'KN',lado+'GT',lado+'AN')
        angles['pie'+side] = kin.angulos(stride,lado+'AN',lado+'KN',lado+'MT')

        angles_abs['brazo'+side] = kin.angulos_abs(stride,lado+'ELB',lado+'SH')
        angles_abs['antbrazo'+side] = kin.angulos_abs(stride,lado+'WR',lado+'ELB')
        angles_abs['muslo'+side] = kin.angulos_abs(stride,lado+'KN',lado+'GT') 
        angles_abs['pierna'+side] = kin.angulos_abs(stride,lado+'AN',lado+'KN')
        angles_abs['pie'+side] = kin.angulos_abs(stride,lado+'MT',lado+'AN')

    angles.reset_index(inplace=True, drop=True)
    angles_abs.reset_index(inplace=True, drop=True)
        
    for col in angles_abs.columns:
        for n,i in enumerate(angles_abs[col]):
            if i<0:
                angles_abs[col][n] = i + 2*np.pi

    dangles = pd.DataFrame()
    dangles_abs = pd.DataFrame()

    for col in angles.columns:
        dangles[col] = sig.convolve(angles[col],win,'same')
        dangles_abs[col] = sig.convolve(angles_abs[col],win,'same')
    dangles.loc[0] = dangles.loc[1]
    dangles.loc[N-1] = dangles.loc[N-2]   
    #    dangles.loc[45] = dangles.loc[44]

    dangles_abs.loc[0] = dangles_abs.loc[1]
    dangles_abs.loc[N-1] = dangles_abs.loc[N-2]   
    #dangles_abs.loc[45] = dangles_abs.loc[44]
    I = pd.DataFrame()

    for segm in angles.columns:    
        I[segm] = (float(m[segm]))*(k[segm]**2)
        cinetica[segm+'Rot'] = (1/2)*I[segm]*(dangles_abs[segm]**2)
 
    cineticatotal = pd.DataFrame()
        
    cineticatotal['musloR'] = cinetica[['muslo_der_z','muslo_der_y','muslo_der_x','muslo_derRot']].sum(axis=1)
    cineticatotal['musloL'] = cinetica[['muslo_izq_z','muslo_izq_y','muslo_izq_x','muslo_izqRot']].sum(axis=1)
    cineticatotal['piernaR'] = cinetica[['pierna_der_z','pierna_der_y','pierna_der_x','pierna_derRot']].sum(axis=1)
    cineticatotal['piernaL'] = cinetica[['pierna_izq_z','pierna_izq_y','pierna_izq_x','pierna_izqRot']].sum(axis=1)
    cineticatotal['pieR'] = cinetica[['pie_der_z','pie_der_y','pie_der_x','pie_derRot']].sum(axis=1)
    cineticatotal['pieL'] = cinetica[['pie_izq_z','pie_izq_y','pie_izq_x','pie_izqRot']].sum(axis=1)
    cineticatotal['brazoR'] = cinetica[['brazo_der_z','brazo_der_y','brazo_der_x','brazo_derRot']].sum(axis=1)
    cineticatotal['brazoL'] = cinetica[['brazo_izq_z','brazo_izq_y','brazo_izq_x','brazo_izqRot']].sum(axis=1)
    cineticatotal['antbrazoR'] = cinetica[['antbrazo_der_z','antbrazo_der_y','antbrazo_der_x','antbrazo_derRot']].sum(axis=1)
    cineticatotal['antbrazoL'] = cinetica[['antbrazo_izq_z','antbrazo_izq_y','antbrazo_izq_x','antbrazo_izqRot']].sum(axis=1)
#    cineticatotal['troncoR'] = cinetica[['tronco_der_z','tronco_der_y','tronco_der_x','tronco_derRot']].sum(axis=1)
#    cineticatotal['troncoL'] = cinetica[['tronco_izq_z','tronco_izq_y','tronco_izq_x','tronco_izqRot']].sum(axis=1)


    cineticaBylimb = pd.DataFrame()
    cineticaBylimb['Wint_ULL'] = cineticatotal[['brazoL','antbrazoL']].sum(axis=1)
    cineticaBylimb['Wint_ULR'] = cineticatotal[['brazoR','antbrazoR']].sum(axis=1)
    cineticaBylimb['Wint_LLR'] = cineticatotal[['musloR','piernaR','pieR']].sum(axis=1)
    cineticaBylimb['Wint_LLL'] = cineticatotal[['musloL','piernaL','pieL']].sum(axis=1)
#    cineticaBylimb['Wint_tr'] = cineticatotal[['troncoR','troncoL']].sum(axis=1)

    incrementos = cineticaBylimb.diff()
    IncrementosPositivos = ((incrementos>0)*incrementos)/(masa*strideLength)
    WintLimb = IncrementosPositivos.sum()
    WintLimbByframe.append(sig.resample(IncrementosPositivos.values[1:,:],100))
    listaWint.append(WintLimb.sum())
    pormiembro = WintLimb.to_frame().T
    pormiembro['Wint'] = WintLimb.sum()
    listLimbdf.append(pormiembro)


    #return listLimbdf
    return pormiembro









