"""
LIBiAM Biomechanics Lab
CENUR Litoral Norte
Universidad de la Republica

Created on Tue May 14 2024 
@author: german and carlo
Depts. of Biological Engineering and Biological Sciences
"""
def browse_folder():
    """Function to select a folder """
    import tkinter as tk
    from tkinter import filedialog
    root = tk.Tk()
    folder_path = filedialog.askdirectory(parent=root, title='Select a folder')  # Open a dialog to browse for folder
    root.destroy()  # Close the window after selection
    return folder_path
    