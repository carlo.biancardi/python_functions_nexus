"""
LIBiAM Biomechanics Lab
CENUR Litoral Norte
Universidad de la Republica

Created on Tue May 14 2024
@author: german and carlo
Depts. of Biological Engineering and Biological Sciences
"""
def load_npy_files(folder_path):
    """Function to load npy files"""
    import os
    npy_files = []
    for file_name in os.listdir(folder_path):
        if file_name.endswith('.npy'):
            npy_files.append(os.path.join(folder_path, file_name))
    return npy_files
    