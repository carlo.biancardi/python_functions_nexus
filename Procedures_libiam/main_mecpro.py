# -*- coding: utf-8 -*-
"""
LIBiAM Biomechanics Lab
CENUR Litoral Norte
Universidad de la Republica


Created on Tue May 12 11:04:18 2024
@authors: german and carlo
Depts. of Biological Engineering and Biological Sciences
"""
import numpy as np  # Required numpy ver 1.20.3
import pandas as pd # Required pandas ver < 2.0.0
import matplotlib.pyplot as plt
from scipy import signal
import glob
import os
import tkinter as tk
from tkinter import filedialog

# Local functions
from utils.load_npy import load_npy_files
from utils.select_folder import browse_folder
from analyses.energetics import energetics

r = []

def main():

    # Select folder w/t npy files
    folder_path = browse_folder()
    if not folder_path:
        print("No folder selected.")
        return

    # Search for npy files and make a list
    npy_files = load_npy_files(folder_path)
    if not npy_files:
        print("No NPY files found in the folder.")
        return

    print("NPY files found:")
    
    # Define results folder
    directory = os.path.join(folder_path, 'results')
    if not os.path.exists(directory):
        os.makedirs(directory)
        
    for file in npy_files:
        
        r = energetics(file,folder_path)

    # print(r)
    alldata = pd.concat(r).reset_index()#.sort_values(by='subjectID')
    df = alldata[['subjectID','BodyMass','gait','vel','veltype','stride','strideFreq','strideLength','Df_right','Df_left','Wext','Wh','Wv','recovery','instantRec','Congruency','Wint','Wint_ULR','Wint_ULL','Wint_LLR','Wint_LLL']].groupby(['subjectID','gait','vel','veltype','stride']).mean().reset_index() #.sort_values(by=['subjectID','vel','gait','stride'])   # carlo 20240517 
    picf = os.path.join(directory,'energetic.pkl')
    csvf = os.path.join(directory,'energetic.csv')
    df.to_pickle(picf)
    df.to_csv(csvf)

if __name__ == "__main__":
    main()
    
