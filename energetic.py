import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy import signal
import glob
import os


directory = './energies_plots/'
if not os.path.exists(directory):
    os.makedirs(directory)

# folder = './'
r = []

# lista = glob.glob('./datosdeprueba/*.npy')
for file in lista:
	file_id = os.path.basename(file)[:-4]	
	#----------------------------------------------------------
	# Carga datos
	data = np.load(file, encoding='latin1',allow_pickle=True).item()
	masa = [s for s in data['info'].columns if "mass" in s]
	m = float(data.get('info')[masa].values.item())
	velocidad = [s for s in data['info'].columns if "vel" in s]
	vel = np.round(float(data.get('info')[velocidad].values.item())/3.6,2)
	veltype = data.get('info')['veltype'].values[0]
	fsample = [s for s in data['info'].columns if "fs_kin" in s]
	fs = float(data.get('info')[fsample].values.item())
	dt = 1/fs
	gait = data.get('info')['gait'].values[0]
	subjectID = data.get('info')['subjectID'].values[0]
	#----------------------------------------------------------
	
	#----------------------------------------------------------
	# Crea ventana de convolución para cálculo de velocidad
	win = np.array([1/(2*dt),0,-1/(2*dt)])
	#----------------------------------------------------------
	
	dfs=[]
	energies_dir = './energies_plots/energies_dir/'
	if not os.path.exists(energies_dir):
	    os.makedirs(energies_dir)
	for n,ii in enumerate(data.get('model_outputs')):
		#----------------------------------------------------------
		# Carga datos del centro de masa
		cm = ii[['cm_x','cm_y','cm_z']].reset_index(drop=True)/1000
		cm.cm_z = cm.cm_z.subtract(cm.cm_z.min())
		#----------------------------------------------------------

		#----------------------------------------------------------
		# Calcula velocidad del centro de masa
		dcm = pd.DataFrame()
		dcm['x'] = signal.convolve(cm['cm_x'],win,'same')
		dcm['y'] = signal.convolve(cm['cm_y'],win,'same')
		# dcm.y = dcm.y.subtract(dcm.y.min())
		dcm['z'] = signal.convolve(cm['cm_z'],win,'same')
		dcm.iloc[0] = dcm.iloc[1]
		dcm.iloc[-1] = dcm.iloc[-2]
		#----------------------------------------------------------

		#----------------------------------------------------------
		# Calcula parámetros espaciotemporales
		stride_duration = cm.shape[0]*dt
		strideFreq = 1/stride_duration
		strideLength = vel/strideFreq
		#----------------------------------------------------------

		#----------------------------------------------------------
		# Cálculo de enegías
		E = pd.DataFrame()
		E['Ekx'] = m*(dcm['x']**2)/2
		E['Ekx'] = E['Ekx'] - E['Ekx'].min()
		
		E['Eky'] = (m*(dcm['y']+vel)**2)/2
		E['Eky'] = E['Eky'] - E['Eky'].min()
		
		E['Ekz'] = (m*(dcm['z']**2))/2
		E['Ekz'] = E['Ekz'] - E['Ekz'].min()
		
		E['mgh'] = m*9.8*(cm['cm_z'])
		E['mgh'] = E['mgh'] - E['mgh'].min()
		
		E['Ev'] =  E['mgh'] + E['Ekz']
		E['Eh'] = E['Ekx'] + E['Eky']
		
		E['Etot'] = E['Ev'] + E['Eh']
		#----------------------------------------------------------

		#----------------------------------------------------------
		# Dibuja energias
		plot = E[['Etot','Ev','Eh']].plot()
		fig = plot.get_figure()
		fig.savefig("energies_"+file_id+str(n+1)+".png")
		#----------------------------------------------------------

		#----------------------------------------------------------
		# Cálculo de Wext, Wh, Wv y recovery
		W = pd.DataFrame()
		wtot = E['Etot'].diff()
		wtot.iloc[0] = wtot.iloc[1]
		
		wz = E['Ev'].diff()
		wz.iloc[0] = wz.iloc[1]
		
		wy = E['Eh'].diff()
		wy.iloc[0] = wy.iloc[1]

		W['Wtot'] = wtot*(wtot>0)
		W['Wz'] = wz*(wz>0)
		W['Wy'] = wy*(wy>0)

		Wext = W.Wtot.sum()/(strideLength*m)
		Wv = W.Wz.sum()/(strideLength*m)
		Wh = W.Wy.sum()/(strideLength*m)
		R = (1-(Wext/(Wv+Wh)))*100
		#----------------------------------------------------------
		energetic = pd.DataFrame()
		energetic['recovery'] = [R]
		energetic['Wext'] = [Wext]
		energetic['Wh'] = [Wh]
		energetic['Wv'] = [Wv]
		energetic['subjectID'] = [subjectID]
		energetic['vel'] = [vel]
		energetic['veltype'] = [veltype]
		energetic['gait'] = [gait]
		energetic['stride'] = [n]
		energetic['strideFreq'] = [strideFreq]
		energetic['strideLength'] = [strideLength]
		#----------------------------------------------------------
		dfs.append(energetic)
	# datas = pd.concat(dfs,ignore_index=True).pivot_table(index=['subjectID','gait','vel'])
	datas = pd.concat(dfs,ignore_index=True)
	r.append(datas)
alldata = pd.concat(r).reset_index()#.sort_values(by='subjectID')
df = alldata[['subjectID','vel','gait','veltype','stride','recovery','Wext','Wh','Wv','strideFreq','strideLength']].groupby(['subjectID','gait','vel','veltype']).mean().reset_index()#.sort_values(by=['subjectID','vel','gait','stride'])
df.to_pickle('./energetic.pickle')
df.to_csv('./energetic.csv')