import sys
import math
import os
import viconnexusapi
from viconnexusapi import ViconNexus, ViconUtils
import ViconNexus as vn

import pandas as pd
from detect_peaks import detect_peaks
import matplotlib.pyplot as plt
import numpy as np

vicon= vn.ViconNexus()
name = vicon.GetSubjectNames()[0]
outputname=vicon.GetModelOutputNames(name)   #ángulos
del outputname[-7:]  #borra velocidades y aceleraciones dado que tienen diferente dimensi'on y no permiten crear un array (lo ideal ser'ia crear un dict porq permite diferentes dim)
cm,e=vicon.GetModelOutput(name,'cm')
b=vicon.GetMarkerNames(name)          #marcadores
path,file_name= vicon.GetTrialName()
fs_kin = vicon.GetFrameRate()
fs_emg = vicon.GetDeviceDetails(1)[2]
ratio_fs = fs_emg/fs_kin


#Agregar fsample para EMG


def get_emg(minimos):
  emg = pd.DataFrame()
  emg_cycles=[]
  # muscle_names = vicon.GetDeviceOutputDetails(1,1)[4]
  # channels = vicon.GetDeviceOutputDetails(1,1)[5]
  
  # muscle_names = vicon.GetDeviceOutputDetails(1,1)[4]
  Nchannels = vicon.GetDeviceDetails(1)[3]
  # for muscle_name,channel in zip(muscle_names,channels):
    # vicon.GetDeviceChannelGlobal(3,12,channels[3])[0]     canal 3 de delsys, musculo 12
  for i,channel in enumerate(Nchannels):
    emg['EMG'+str(i+1) ]=vicon.GetDeviceChannelGlobal(1,i+1,1)[0]

  for i in range(len(minimos[:-2])):
    start = int(minimos[i]*ratio_fs)
    finish = int(minimos[i+1]*ratio_fs)
    emg_cycles.append(emg.iloc[start:finish,:])


  # EMG = emg.loc[(emg!=0).any(axis=1)]
  return emg_cycles#, EMG

directory = path+'npy\\'
data = np.load(directory+file_name+'.npy',allow_pickle=True).item()
minimos = data['minimos']

emg = get_emg(minimos)
data.update({'emg':emg})
np.save(directory+file_name+'.npy',data)

directoryname = path+'csv\\'+file_name
for n,i in enumerate(emg):
  i.to_csv(directoryname+'//' +file_name+'_emg'+str(n+1)+'.csv')