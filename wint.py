# Biomecanica trabajo final


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import scipy.signal as sig
import kinematics as kin
import glob
import seaborn as sns
from progress.bar import FillingCirclesBar
import os


trials = './data/*.npy'
carpeta = glob.glob(trials)
# borrar = [s for s in carpeta if "GP_R_065" in s][0]
# carpeta.remove(borrar)
bar = FillingCirclesBar('Processing', max=len(carpeta))
subtrials =  ['./00_W_068.npy']
#cargo archivo de datos

listdf = []
Wintbyframe = {} 


segmentos = ['brazo_der', 'brazo_izq', 
             'antbrazo_der','antbrazo_izq', 
             'muslo_der','muslo_izq',
             'pierna_der','pierna_izq', 
             'pie_der','pie_izq']

segmentosXYZ = ['pierna_der_x',  'pierna_der_y', 'pierna_der_z', 
                'pierna_izq_x', 'pierna_izq_y', 'pierna_izq_z',
                'muslo_der_x', 'muslo_der_y', 'muslo_der_z', 
                'muslo_izq_x', 'muslo_izq_y', 'muslo_izq_z',
                'pie_der_x', 'pie_der_y', 'pie_der_z',
                'pie_izq_x', 'pie_izq_y', 'pie_izq_z',
                'brazo_der_x', 'brazo_der_y', 'brazo_der_z', 
                'brazo_izq_x', 'brazo_izq_y', 'brazo_izq_z',
                'antbrazo_der_x', 'antbrazo_der_y', 'antbrazo_der_z', 
                'antbrazo_izq_x', 'antbrazo_izq_y', 'antbrazo_izq_z']
SegmentosDerechos = [s for s in segmentosXYZ if 'der' in s]
SegmentosIzquierdos = [s for s in segmentosXYZ if 'izq' in s]

for trial in carpeta:
    data = np.load(trial,allow_pickle=True,encoding='latin1').item()
    info = data['info']
    file_name = os.path.basename(trial)[:-4]
    fs = [s for s in data['info'].columns if "fs" in s]
    masa=data['info']['mass'].astype(float)[0]
    m=pd.DataFrame(data = {'brazo_der':[masa*0.028],
                               'brazo_izq':[masa*0.028],
                               'antbrazo_der':[masa*0.016],
                               'antbrazo_izq':[masa*0.016],
                               'muslo_der':[masa*0.100],
                               'muslo_izq':[masa*0.100],
                               'pierna_der':[masa*0.0465],
                               'pierna_izq':[masa*0.0465],
                               'pie_der':[masa*0.0145],
                               'pie_izq':[masa*0.0145]})
    # Defino dt y ventana de convolucion
    fs_kin = info[fs].values[0][0]
    dt = float(1/fs_kin)
    win = np.array([1/(2*dt),0,-1/(2*dt)])
    #vel = strideFreq*strideLen
    vel = np.round(float(info['vel'])/3.6,2)
    
    listaWint = []
    listLimbdf = []
    bar.next()

    # if info.trailing_leg.values[0].lower() == 'r':
    #     trail = 'r'
    #     trasera = 'der'
    #     lead = 'l'
    #     delantera = 'izq'
    #     SegTra = SegmentosDerechos
    #     SegDel = SegmentosIzquierdos
    # else:
    #     trail = 'l'
    #     trasera = 'der'
    #     lead = 'r'
    #     delantera = 'izq'
    #     SegDel = SegmentosDerechos
    #     SegTra = SegmentosIzquierdos

    WintLimbByframe = []
    
    for paso, marcadores in zip(data['model_outputs'],data['kinematic']):
    
        N = paso.shape[0]
        strideDur = N*dt
        strideFreq = 1/strideDur
        strideLen = vel/strideFreq
        marcadores =marcadores/1000
        k=pd.DataFrame(data = {'brazo_der':[0.542]*np.sqrt((marcadores['RELBx']-marcadores['RSHx'])**2+(marcadores['RELBy']-marcadores['RSHy'])**2+(marcadores['RELBz']-marcadores['RSHz'])**2),
                               'brazo_izq':[0.542]*np.sqrt((marcadores['LELBx']-marcadores['LSHx'])**2+(marcadores['LELBy']-marcadores['LSHy'])**2+(marcadores['LELBz']-marcadores['LSHz'])**2),
                               'antbrazo_der':[0.827]*np.sqrt((marcadores['RWRx']-marcadores['RELBx'])**2+(marcadores['RWRy']-marcadores['RELBy'])**2+(marcadores['RWRz']-marcadores['RELBz'])**2),
                               'antbrazo_izq':[0.827]*np.sqrt((marcadores['LWRx']-marcadores['LELBx'])**2+(marcadores['LWRy']-marcadores['LELBy'])**2+(marcadores['LWRz']-marcadores['LELBz'])**2),
                               'muslo_der':[0.540]*np.sqrt((marcadores['RKNx']-marcadores['RGTx'])**2+(marcadores['RKNy']-marcadores['RGTy'])**2+(marcadores['RKNz']-marcadores['RGTz'])**2),
                               'muslo_izq':[0.540]*np.sqrt((marcadores['LKNx']-marcadores['LGTx'])**2+(marcadores['LKNy']-marcadores['LGTy'])**2+(marcadores['LKNz']-marcadores['LGTz'])**2),
                               'pierna_der':[0.528]*np.sqrt((marcadores['RANx']-marcadores['RKNx'])**2+(marcadores['RANy']-marcadores['RKNy'])**2+(marcadores['RANz']-marcadores['RKNz'])**2),
                               'pierna_izq':[0.528]*np.sqrt((marcadores['LANx']-marcadores['LKNx'])**2+(marcadores['LANy']-marcadores['LKNy'])**2+(marcadores['LANz']-marcadores['LKNz'])**2),
                               'pie_der':[0.690]*np.sqrt((marcadores['RMTx']-marcadores['RHEEx'])**2+(marcadores['RMTy']-marcadores['RHEEy'])**2+(marcadores['RMTz']-marcadores['RHEEz'])**2),
                               'pie_izq':[0.690]*np.sqrt((marcadores['LMTx']-marcadores['LHEEx'])**2+(marcadores['LMTy']-marcadores['LHEEy'])**2+(marcadores['LMTz']-marcadores['LHEEz'])**2)})
        k.reset_index(inplace=True, drop=True)

        relativeCm = pd.DataFrame()
        relativeVelocity = pd.DataFrame()

        for seg in [SegmentosDerechos,SegmentosIzquierdos]:
            for se in seg:
                relativeCm[se] = (paso['cm_'+se] - paso['cm_'+se[-1]])/1000
                relativeVelocity[se] = sig.convolve(relativeCm[se],win,'same')
        relativeCm.reset_index(inplace=True, drop=True)
        
        relativeVelocity.iloc[0] = relativeVelocity.iloc[1]
        relativeVelocity.iloc[N-1] = relativeVelocity.iloc[N-2]
        relativeVelocity.reset_index(inplace=True, drop=True)

        cinetica  = pd.DataFrame()

        for segXYZ in relativeVelocity.columns:
            cinetica[segXYZ] = (1/2)*((relativeVelocity[segXYZ]**2)*float(m[segXYZ[:-2]]))
        cinetica.reset_index(inplace=True, drop=True)


        angles = pd.DataFrame()
        angles_abs = pd.DataFrame()

        for lado,side in zip(['R','L'],['_der','_izq']):
            angles['brazo'+side] = kin.angulos(marcadores,lado+'SH',lado+'ELB',lado+'GT')
            angles['antbrazo'+side] = kin.angulos(marcadores,lado+'ELB',lado+'SH',lado+'WR')
            angles['muslo'+side] = kin.angulos(marcadores,lado+'GT',lado+'SH',lado+'KN') 
            angles['pierna'+side] = kin.angulos(marcadores,lado+'KN',lado+'GT',lado+'AN')
            angles['pie'+side] = kin.angulos(marcadores,lado+'AN',lado+'KN',lado+'MT')

            angles_abs['brazo'+side] = kin.angulos_abs(marcadores,lado+'ELB',lado+'SH')
            angles_abs['antbrazo'+side] = kin.angulos_abs(marcadores,lado+'WR',lado+'ELB')
            angles_abs['muslo'+side] = kin.angulos_abs(marcadores,lado+'KN',lado+'GT') 
            angles_abs['pierna'+side] = kin.angulos_abs(marcadores,lado+'AN',lado+'KN')
            angles_abs['pie'+side] = kin.angulos_abs(marcadores,lado+'MT',lado+'AN')

        angles.reset_index(inplace=True, drop=True)
        angles_abs.reset_index(inplace=True, drop=True)
        
        for col in angles_abs.columns:
            for n,i in enumerate(angles_abs[col]):
                if i<0:
                    angles_abs[col][n] = i + 2*np.pi


        dangles = pd.DataFrame()
        dangles_abs = pd.DataFrame()

        for col in angles.columns:
            dangles[col] = sig.convolve(angles[col],win,'same')
            dangles_abs[col] = sig.convolve(angles_abs[col],win,'same')
        dangles.loc[0] = dangles.loc[1]
        dangles.loc[N-1] = dangles.loc[N-2]

        dangles_abs.loc[0] = dangles_abs.loc[1]
        dangles_abs.loc[N-1] = dangles_abs.loc[N-2]
        I = pd.DataFrame()

        for segm in m.columns:    
            I[segm] = (float(m[segm]))*(k[segm]**2)
            cinetica[segm+'Rot'] = (1/2)*I[segm]*(dangles_abs[segm]**2)
 
        cineticatotal = pd.DataFrame()
        
        cineticatotal['musloR'] = cinetica[['muslo_der_z','muslo_der_y','muslo_derRot']].sum(axis=1)#,'brazo_derRot_x']].sum(axis=1)
        cineticatotal['musloL'] = cinetica[['muslo_izq_z','muslo_izq_y','muslo_izqRot']].sum(axis=1)#,'brazo_izqRot_x']].sum(axis=1)
        cineticatotal['piernaR'] = cinetica[['pierna_der_z','pierna_der_y','pierna_derRot']].sum(axis=1)#,'antbrazo_derRot_x']].sum(axis=1)
        cineticatotal['piernaL'] = cinetica[['pierna_izq_z','pierna_izq_y','pierna_izqRot']].sum(axis=1)#,'antbrazo_izqRot_x']].sum(axis=1)
        cineticatotal['pieR'] = cinetica[['pie_der_z','pie_der_y','pie_derRot']].sum(axis=1)#,'antbrazo_derRot_x']].sum(axis=1)
        cineticatotal['pieL'] = cinetica[['pie_izq_z','pie_izq_y','pie_izqRot']].sum(axis=1)
        cineticatotal['brazoR'] = cinetica[['brazo_der_z','brazo_der_y','brazo_derRot']].sum(axis=1)#,'brazo_derRot_x']].sum(axis=1)
        cineticatotal['brazoL'] = cinetica[['brazo_izq_z','brazo_izq_y','brazo_izqRot']].sum(axis=1)#,'brazo_izqRot_x']].sum(axis=1)
        cineticatotal['antbrazoR'] = cinetica[['antbrazo_der_z','antbrazo_der_y','antbrazo_derRot']].sum(axis=1)#,'antbrazo_derRot_x']].sum(axis=1)
        cineticatotal['antbrazoL'] = cinetica[['antbrazo_izq_z','antbrazo_izq_y','antbrazo_izqRot']].sum(axis=1)#,'antbrazo_izqRot_x']].sum(axis=1)


        cineticaBylimb = pd.DataFrame()
        cineticaBylimb['supL'] = cineticatotal[['brazoL','antbrazoL']].sum(axis=1)
        cineticaBylimb['supR'] = cineticatotal[['brazoR','antbrazoR']].sum(axis=1)
        cineticaBylimb['infR'] = cineticatotal[['musloR','piernaR','pieR']].sum(axis=1)
        cineticaBylimb['infL'] = cineticatotal[['musloL','piernaL','pieL']].sum(axis=1)

 
        incrementos = cineticaBylimb.diff()
        IncrementosPositivos = ((incrementos>0)*incrementos)/(masa*strideLen)
        WintLimb = IncrementosPositivos.sum()
        WintLimbByframe.append(sig.resample(IncrementosPositivos.values[1:,:],100))
        listaWint.append(WintLimb.sum())
        pormiembro = WintLimb.to_frame().T
        pormiembro['Wint'] = WintLimb.sum()
        listLimbdf.append(pormiembro)

    allstrides = pd.concat(listLimbdf,ignore_index=True)
    WINT = pd.DataFrame()
    WINT = allstrides.mean().to_frame().T
    byframe = pd.DataFrame(np.mean(WintLimbByframe,axis=0),columns=IncrementosPositivos.columns)
    Wintbyframe.update({file_name:byframe})

    WINT['subjectID'] = [info.subjectID.values[0]]
    WINT['vel'] = [vel]
    # WINT['gait'] = [info.gait[0].upper()]
    listdf.append(WINT)
bar.finish()
Wint = pd.concat(listdf,ignore_index=True)
Wint.to_pickle('./wint.pickle')
Wint.to_csv('./wint.csv')
