import sys
import math
import os
import viconnexusapi
from viconnexusapi import ViconNexus, ViconUtils
import ViconNexus as vn

import pandas as pd
from detect_peaks import detect_peaks
import matplotlib.pyplot as plt
import numpy as np
import tkinter as tk
from tkinter import messagebox
from tkinter import simpledialog
import scipy.signal as sig

vicon= vn.ViconNexus()
name = vicon.GetSubjectNames()[0]
outputname=vicon.GetModelOutputNames(name)   #ángulos
del outputname[-7:]  #borra velocidades y aceleraciones dado que tienen diferente dimensi'on y no permiten crear un array (lo ideal ser'ia crear un dict porq permite diferentes dim)
cm,e=vicon.GetModelOutput(name,'cm')
b=vicon.GetMarkerNames(name)          #marcadores
path,file_name= vicon.GetTrialName()

#Funcion para interfaz---------------------------------------------------------
def get_selected_item():
    global selected_item_variable  # Declara la variable como global
    selected_item = listbox.get(tk.ANCHOR)
    selected_item_variable = selected_item  # Guarda el elemento seleccionado en la variable global
    selected_item_label.config(text="Selected Item: " + selected_item)
root = tk.Tk()# Crea un Listbox
listbox = tk.Listbox(root, selectmode=tk.SINGLE)
listbox.pack()

#-------------------------------------------------------------------------------
#Interfaz para elegir marcador y segmentar
for item in b:
    listbox.insert(tk.END, item)# Crea un botón para obtener el elemento seleccionado
select_button = tk.Button(root, text="Get Selected Item", command=get_selected_item)
select_button.pack()# Crea una etiqueta para mostrar el elemento seleccionado
selected_item_label = tk.Label(root, text="Selected Item: ")
selected_item_label.pack()# Inicia el bucle principal
root.mainloop()# Aquí puedes acceder a selected_item_variable después de que la ventana principal se haya cerrado
print(selected_item_variable)
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
#Funci'on p[ara detrectar cruce de cero de la velocidad vertical en MTz
def min_marker(name, marker, distance,fs):
  x,y,p,e= vicon.GetTrajectory(name, marker)
  p = np.array(p)
  s = p[e]

  dt = 1/fs

  win = np.array([1/(2*dt),0,-1/(2*dt)])
  ds = sig.convolve(s,win,'same')
  ds[0] = ds[1]
  ds[-1] = ds[-2]

  ipeaks = detect_peaks(ds,mpd=distance,valley=True,show=True)

  lista = []
  for i in ipeaks[1:-1]:
    m = ds[i:i+20]
    lista.append(np.abs(m).argmin()+i)
  min_t = np.array(lista)

  plt.plot(ds)
  plt.plot(min_t,ds[min_t],'r*')
  plt.plot([0,len(ds)],[0,0],'-b')
  plt.title('# strides = ' + str(len(min_t)))
  plt.show()
  return min_t

#-------------------------------------------------------------------------------
#Funci'on para segmentar datos de marcadores
def kinematic(subject, markers, min_t_r, e):
  kinematic = pd.DataFrame()
  for i in range(len(markers)):
    x,y,z,e= vicon.GetTrajectory(name, markers[i])
    x = np.array(x)
    x = x[e]
    y = np.array(y)
    y = y[e]
    z = np.array(z)
    z = z[e]
    inicio = e.index(True)+1
    kinematic[markers[i]+'x'] = x
    kinematic[markers[i]+'y'] = y
    kinematic[markers[i]+'z'] = z 

  kinematic_cycles_r = []

  for i in range(len(min_t_r[1:-1])):
    start = inicio+min_t_r[i]
    fin = inicio+min_t_r[i+1]
    kinematic_cycles_r.append(kinematic.iloc[start:fin,:])
  return kinematic_cycles_r
#-------------------------------------------------------------------------------
# Define a function to ask for user input using Tkinter
def ask_user_input():
    global distance
    new_value = simpledialog.askfloat("Input", "Enter the new value:")
    if new_value is not None:
        distance = new_value
        print("Value updated successfully.")

# --------------------------------------------

#fs_emg = vicon.GetDeviceDetails(1)[2]
fs_kin = vicon.GetFrameRate()
#print(fs_emg)
print(fs_kin)

distance = 60
# Define a flag to indicate whether the value is okay
value_okay = False

# Loop until the value is okay
# Create a Tkinter window
# root = tk.Tk()
while not value_okay:
    minimos = min_marker(name, selected_item_variable, distance, fs_kin)
    #  A = messagebox.askquestion("Question","Distance is {distance}: Are you ok with this segmentation?")
    A = messagebox.askyesno("Value Check", f"The current value is {distance}. Is this value okay?")
    if not A:
        # print("distance :", distance)
        # new_value = input("Enter the new value for distance: ")
        # new_value = simpledialog.askfloat("Input", "Enter the new value:")
        ask_user_input()
        #try:
        #    # Try converting the input to a numeric value
        #    distance = float(new_value)
        #    print("Value updated successfully.")
        #except ValueError:
        #    print("Invalid input. Please enter a numeric value.")
    else: 
        # If the user says the value is okay, set the flag to True to exit the loop
        print("Value is okay.")
        value_okay = True
            
mocap = kinematic(name, b, minimos, e)
data = dict()


for i in range(5):
    name, tipo, rate, deviceOutputIDs, forceplate, eyetracker = vicon.GetDeviceDetails(i)
    DeviceDisplay = 'i == ' + str(i) + ' DeviceID: {0} is named {1} and it is a {2} device' .format(deviceOutputIDs, name, tipo )
    print(DeviceDisplay)

info = pd.DataFrame([{'fs_kin':fs_kin}])
data = ({'file_name':file_name,'kinematic':mocap, 'info':info, 'minimos':minimos})

directory = path+'npy\\'
if not os.path.exists(directory):
    os.makedirs(directory)

directorycsv = path+'csv\\'
if not os.path.exists(directorycsv):
    os.makedirs(directorycsv)

directoryname = path+'csv\\'+file_name
if not os.path.exists(directoryname):
    os.makedirs(directoryname)

np.save(directory+file_name+'.npy',data)

for n,i in enumerate(mocap):
    i.to_csv(directoryname+'//' +file_name+'_markers'+str(n+1)+'.csv')

