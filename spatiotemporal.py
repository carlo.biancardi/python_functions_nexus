import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy import signal
import glob
import os
import pickle
from progress.bar import FillingCirclesBar

print('Loading data...')
dic = pickle.load(open("./data/data.pickle", "rb"))
DFS = []
conds = list(dic.keys())
print('Processing data...')
conds = [s for s in list(dic.keys()) if 'W' not in s[3]]
bar = FillingCirclesBar('Processing', max=len(conds))
for file_id in conds:
	trial = dic[file_id]
	bar.next()
	# file_id = os.path.basename(file)[:-4]
	for kinematic in ['marker_tr','marker_ld']:
		info = trial['info']
		# print(file_id)
		data = trial[kinematic]
		m = info['mass (kg)'].values.item()
		trLeg = info['trailing_leg'].values.item()
		vel = info['vel (km/h)'].values.item()
		fsample = [s for s in list(info.columns) if "fs_kin" in s]
		fs = info[fsample].values.item()
		dt = 1/fs
		# subject = info['file_name'].values.item()[:2].upper()
		gait = info['gait'].values.item().upper()
		lista1 = []
		lista2 = []
		lista3 = []
		lista4 = []
		lista5 = []
		n_pasos = len(data)
		pasos = int(np.round(n_pasos)/2)-10
		for ii,stride in enumerate(range(pasos,pasos+20)):
			marcadores = list(data[stride].columns) 
			talon = [s for s in marcadores if 'MTz' in s]
			s = data[stride][talon]
			if len(s)>20:
				idxInicio = s.index[0]
				idxFin = s.index[-1]
				minimo = s.idxmin()
				maximo = s.idxmax()
				threshold = s.loc[minimo].values[0][0] + 20
				# sCutted = s.iloc[10:]
				sCutted = s.iloc[:-20]
				# if not sCutted.empty:
				idxToeoff = sCutted[sCutted<threshold].dropna().index[-1]
				cycle_dur = (idxFin - idxInicio)/fs
				stride_freq = 1/cycle_dur
				stride_len = (vel/3.6)/stride_freq			
				contact = (idxToeoff - idxInicio)/fs
				duty = contact/cycle_dur
				lista1.append(duty)
				lista2.append(contact)
				lista3.append(stride_freq)
				lista4.append(stride_len)
				lista5.append(ii+1)
		# dfs.update({side:np.mean(lista1)}) 
		# contacts.update({side:np.mean(lista2)})
		# freqs.update({side:np.mean(lista3)})
		# lengths.update({side:np.mean(lista4)})
		n_strides = len(lista5)
		DF = pd.DataFrame()
		DF['subject'] = [file_id[:2]]*n_strides
		DF['gait'] = [gait]*n_strides
		DF['vel'] = [vel]*n_strides
		DF['leg'] = [kinematic[-2:]]*n_strides
		DF['stride'] = lista5
		DF['dutyFactor'] = lista1
		DF['contactsTime'] = lista2
		DF['stride_freq'] = lista3
		DF['stride_len'] = lista4
		DFS.append(DF)
dutyFactor = pd.concat(DFS,ignore_index=True,sort=False)
dutyFactor.to_pickle('./data/sp.pickle')
dutyFactor.to_csv('./data/sp.csv')
bar.finish()