import sys
import math
import os
import viconnexusapi
from viconnexusapi import ViconNexus, ViconUtils
import ViconNexus as vn

import pandas as pd
from detect_peaks import detect_peaks
import matplotlib.pyplot as plt
import numpy as np

vicon= vn.ViconNexus()
name = vicon.GetSubjectNames()[0]
outputname=vicon.GetModelOutputNames(name)   #ángulos
del outputname[-7:]  #borra velocidades y aceleraciones dado que tienen diferente dimensi'on y no permiten crear un array (lo ideal ser'ia crear un dict porq permite diferentes dim)
cm,e=vicon.GetModelOutput(name,'cm')
b=vicon.GetMarkerNames(name)          #marcadores
path,file_name= vicon.GetTrialName()

# def min_marker(name, marker, distance):
#   x,y,p,e= vicon.GetTrajectory(name, marker)
#   p = np.array(p)
#   m = p[e]
#   min_t= detect_peaks(m, mpd = distance,show=True,valley=True, mph = 1400)
#   return min_t

def get_modeloutputs(subject, min_t_r, e):
  model_output = pd.DataFrame()
  for out in outputname:
    print(out)
    ch,e = vicon.GetModelOutput(name,out)
    x = np.array(ch[0])
    x = x[e]
    model_output[out+'_x'] = np.array(x)
    # if len(ch)>1:
    y = np.array(ch[1])
    y = y[e]
    z = np.array(ch[2])
    z = z[e]
    model_output[out+'_y'] = np.array(y)
    model_output[out+'_z'] = np.array(z)
  model_output_cycles = []
  for i in range(len(min_t_r[1:-1])):
    model_output_cycles.append(model_output.iloc[min_t_r[i]:min_t_r[i+1],:])
 
  
  return model_output_cycles




# # vel = np.float(input('Enter speed in km/h : '))
fs_kin = vicon.GetFrameRate()

# minimos = min_marker(name, b[3], 50)
directory = path+'npy\\'
data = np.load(directory+file_name+'.npy',allow_pickle=True).item()
minimos = data['minimos']
modoutputs = get_modeloutputs(name, minimos, e)


data.update({'model_outputs':modoutputs})

np.save(directory+file_name+'.npy',data)

directoryname = path+'csv\\'+file_name
for n,i in enumerate(modoutputs):
  i.to_csv(directoryname+'//' +file_name+'_model_outputs'+str(n+1)+'.csv')