import sys
import math
import os
import viconnexusapi
from viconnexusapi import ViconNexus, ViconUtils
import ViconNexus as vn

import numpy as np
import PySimpleGUI as sg
import pandas as pd

vicon= vn.ViconNexus()
name = vicon.GetSubjectNames()[0]
outputname=vicon.GetModelOutputNames(name)   #ángulos
del outputname[-7:]  #borra velocidades y aceleraciones dado que tienen diferente dimensi'on y no permiten crear un array (lo ideal ser'ia crear un dict porq permite diferentes dim)
cm,e=vicon.GetModelOutput(name,'cm')
b=vicon.GetMarkerNames(name)          #marcadores
path,file_name= vicon.GetTrialName()
directory = path+'npy\\'
directorycsv = path+'csv\\'
data = np.load(directory+file_name+'.npy',allow_pickle=True).item()



sg.theme('Dark Amber')  # Let's set our own color theme

# STEP 1 define the layout
layout = [
            [sg.Text('Trial Info')],
            [sg.Text('Subject_ID'), sg.Input()],
            [sg.Text('Subject'), sg.Input()],
            [sg.Text('Mass (kg)'), sg.Input()],
            [sg.Text('Height (m)'),sg.Input()],
            [sg.Text('Age (years old)'),sg.Input()],
            [sg.Text('vel (km/h)'), sg.Input()],
            [sg.Text('Gait'), sg.Input()],
            [sg.Text('vel(1,2,3)'), sg.Input()],
            [sg.Button('Ok')]
         ]

#STEP 2 - create the window
window = sg.Window('Trial info', layout)

# STEP3 - the event loop
while True:
    event, values = window.read()   # Read the event that happened and the values dictionary
    print(event, values)
    if event == sg.WIN_CLOSED or event == 'Ok':     # If user closed window with X or if user clicked "Exit" button then exit
      break
#    if event == 'Button':
#      print('You pressed the button')
window.close()

columns = ['subjectID','subject','mass','height','age','vel','gait','veltype']
info = pd.DataFrame(values,index=[0])
info.columns=columns
info['fsample'] = vicon.GetFrameRate()
data.update({'info':info})
info.to_csv(directorycsv+'info_'+file_name+'.csv')
np.save(directory+file_name+'.npy',data)