import pandas as pd
import matplotlib.pyplot as plt
import scipy.signal as sig
import numpy as np
import detect_peaks

''' Este código detecta el toque en el piso cuando la velocidad del marcador MTz == 0'''


fs = 100
dt = 1/fs

s = pd.read_pickle('./MTz.pickle').to_numpy()
win = np.array([1/(2*dt),0,-1/(2*dt)])
ds = -sig.convolve(s,win,'same')
ds[0] = ds[1]
ds[-1] = ds[-2]

ipeaks = detect_peaks.detect_peaks(ds,mpd=50,valley=True)
lista = []
for i in ipeaks[1:-1]:
	m = ds[i:i+20]
	lista.append(np.abs(m).argmin()+i)
ihs = np.array(lista)


plt.plot(ds)
plt.plot(ihs,ds[ihs],'r*')
plt.plot([0,len(ds)],[0,0],'-b')
plt.title('# strides = ' + str(len(ihs)))
plt.show()

